import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Methods {
    Scanner in = new Scanner(System.in);
    Map<String, String> user = new HashMap<>();
    boolean running = true;
    boolean loggedOut = true;


    public void run() {

        System.out.println("""
                                
                Witam Cie w prostym programie 
                do przetrzymywania danych o pracownikach.
                """);
        System.out.println("""
                Aby nawigować po programie 
                wybieraj odpowiednie opcje.
                """);

        int choosenNumber;
        while (running) {
            while (loggedOut) {
                System.out.println(
                        """
                                1. Utwórz konto
                                2. Zaloguj
                                3. Zamknij""");
                System.out.println("Wybierz opcję: ");
                try {
                    Scanner in = new Scanner(System.in);
                    choosenNumber = in.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("Dokonano niepoprawnego wyboru");
                    choosenNumber = 0;
                }


                switch (choosenNumber) {
                    case 1 -> createAccount();
                    case 2 -> login();
                    case 3 -> exit();
                    default -> inputMismatch();
                }
            }

            while (!loggedOut) {
                int choosenOption = 0;
                System.out.println(
                        """
                                1. Dodaj pracownika
                                2. Wyświetl listę pracowników
                                3. Dodaj notatkę do pracownika
                                4. Wyświetl dane pracownika
                                5. Wyczyść notatki pracownika
                                6. Wyloguj
                                7. Zamknij""");
                System.out.println("Wybierz opcję: ");
                try {
                    Scanner in = new Scanner(System.in);
                    choosenOption = in.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("Dokonano niepoprawnego wyboru");
                }

                switch (choosenOption) {
                    case 1 -> addEmployee();
                    case 2 -> printAllEmployeesList();
                    case 3 -> addNotesToEmployee(employeeName());
                    case 4 -> printEmployeeData(employeeName());
                    case 5 -> clearEmployeeNotes(employeeName());
                    case 6 -> logout();
                    case 7 -> exit();
                    default -> inputMismatch();
                }
            }
        }
    }

    public void createAccount() {

        System.out.println("Podaj login: ");
        String login = in.next();
        if (user.containsKey(login)) {
            System.out.println("Podany login już istnieje");
            createAccount();
        }
        System.out.println("Podaj hasło: ");
        String password = in.next();
        System.out.println("Powtórz hasło: ");
        String passwordCheck = in.next();
        if (password.equals(passwordCheck)) {
            user.put(login, password);
            System.out.println("Konto zostało utworzone.");
        } else {
            System.out.println("Hasła nie są takie same.");
        }

    }

    public boolean login() {
        boolean status = true;


        System.out.println("Podaj login: ");
        String login = in.next();
        System.out.println("Podaj hasło: ");
        String password = in.next();

        if (user.containsKey(login)) {
            if (user.containsValue(password)) {
                System.out.println("Logowanie poprawne");
                status = false;
            }
        } else {
            System.out.println("Logowanie niepoprawne. Sprawdź login i hasło.");

        }
        return loggedOut = status;
    }

    public String employeeName() {

        Scanner in = new Scanner(System.in);
        System.out.println("Podaj imię: ");
        String firstName = in.next();
        System.out.println("Podaj nazwisko: ");
        String lastName = in.next();

        return firstName + " " + lastName;
    }

    public void addEmployee() {
        String employeeList = "";
        System.out.println("Podaj imię: ");
        String firstName = in.next();
        System.out.println("Podaj nazwisko: ");
        String lastName = in.next();

        try {
            FileReader fr = new FileReader("Employees.txt");

        } catch (FileNotFoundException e) {
            try {
                File file = new File("Employees.txt");
                if (file.createNewFile()) {
                    System.out.println("File created");
                } else {
                    System.out.println("File already exists");
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        try {
            FileReader fr = new FileReader("Employees.txt");
            Scanner in = new Scanner(fr);
            while (in.hasNextLine()) {
                employeeList = employeeList + in.nextLine() + "\n";
            }
            if (!employeeList.contains(firstName + " " + lastName)) {
                FileWriter fw = new FileWriter("Employees.txt");
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(employeeList + firstName + " " + lastName);
                bw.close();

                try {
                    File file = new File(firstName + " " + lastName + ".txt");
                    if (file.createNewFile()) {
                        System.out.println("Udało się utworzyć plik nowego pracownika");
                    } else {
                        System.out.println("Tu nawet pewnie nie dojdzie");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Taki pracownik znajduję się już na liście");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addNotesToEmployee(String name) {
        String notes = "";

        Scanner in = new Scanner(System.in);
        try {
            FileReader fr = new FileReader(name + ".txt");
            Scanner sc = new Scanner(fr);
            while (sc.hasNextLine()) {

                notes = notes + "\n" + sc.nextLine();
            }
            try {
                FileWriter fw = new FileWriter(name + ".txt");
                BufferedWriter bw = new BufferedWriter(fw);

                System.out.println("Notatka: \n");
                bw.write(notes + "\n" + in.nextLine());
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pracownika");
        }

    }

    public void printEmployeeData(String name) {

        try {
            FileReader fr = new FileReader(name + ".txt");
            Scanner sc = new Scanner(fr);

            System.out.println("Notatka: ");
            while (sc.hasNextLine()) {
                System.out.println(sc.nextLine());

            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pracownika");
        }
        System.out.println();
    }

    public void clearEmployeeNotes(String name) {
        Path path = Paths.get(name + ".txt");
        try {
            Files.deleteIfExists(Path.of(String.valueOf(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Files.createFile(Path.of(String.valueOf(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean logout() {
        return loggedOut = true;
    }

    public boolean exit() {
        return running = false;
    }

    public void inputMismatch() {
        System.out.println("Dokonano błędnego wyboru");
    }

    public void printAllEmployeesList() {
        try {
            FileReader fr = new FileReader("Employees.txt");
            Scanner sc = new Scanner(fr);
            while (sc.hasNextLine()) {
                System.out.println(sc.nextLine());
            }
            sc.close();
            System.out.println();
        } catch (FileNotFoundException e) {
            try {
                File file = new File("Employees.txt");
                if (file.createNewFile()) {
                    System.out.println("Nie masz jeszcze żadnego pracownika");
                } else {
                    System.out.println("Plik już istnieje");
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}

